import requests
import json
import sys
import configparser

config = configparser.ConfigParser()
config.read('config.ini')
defaultconfig = config['DEFAULT']
creds = config['USER']

if len(sys.argv) > 2:
	datain = sys.argv[1]
	filein = sys.argv[2]
	data=json.loads(datain)

	#input validation can go here

	datastr=json.dumps(data)

	if('case_name' in data):
		files = {
		    'data': datastr,
		    'file': (filein, open(filein, 'rb'))
		}

		responsefile = requests.post(defaultconfig['UrlFile'], files=files, data=data, auth=(creds['User'],  creds['Password']),timeout=3600)
	else:
		print("Incorrect JSON")		
else:
	print("Incorrect params")

# FOR LOCARD INTEGRATION

# import requests

# url = "https://bpm6.locard.motivian.com/locard/rest/externalAgent/submitEvidence"

# payload="{\r\n    \"caseId\":\"16002\"\r\n    \"hash\":\"ABCDEFG\"\r\n}"
# headers = {
#   'Authorization': 'Basic dW5pX21hbDp1bmlfbWFs',
#   'Content-Type': 'text/plain'
# }

# response = requests.request("POST", url, headers=headers, data=payload)

# print(response.text)