BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "workflows" (
        "workflowId"        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        "description"   TEXT,
        "investigator"  TEXT
);
CREATE TABLE IF NOT EXISTS "transactions" (
        "transactionId"        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        "workflowId"        INTEGER NOT NULL,
        "deviceId"   TEXT,
        "highLevelEvent"   TEXT,
        "lowLevelEvent"   TEXT,
        "evidence"   TEXT,
        "evidenceHash"       TEXT,
        "timeOfTransaction"       INTEGER
);
CREATE TABLE IF NOT EXISTS "hashes" (
        "hashId"        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        "transactionId"        INTEGER NOT NULL,
        "hash"   TEXT
);
COMMIT;





